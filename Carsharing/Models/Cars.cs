﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carsharing.Models
{
    public class Cars
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public double GeoLong { get; set; } // долгота - для карт Яндекса
        public double GeoLat { get; set; } // широта - для карт Яндекса
        public string Number { get; set; }
    }
}