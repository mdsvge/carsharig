﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Carsharing.Startup))]
namespace Carsharing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
