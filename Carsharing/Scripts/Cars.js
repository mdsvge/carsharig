﻿ymaps.ready(init)
    function init() {
    var myMap = new ymaps.Map("YMaps", {
        center: [54.30, 48.34],
        controls: ['zoomControl',
            'geolocationControl',
            'fullscreenControl'],
        zoom: 13
    });

};

$.getJSON('@Url.Action("GetData","Home")', function (data) {
    $.each(data, function (item) {
         marker = new ymaps.GeoObject({
            geometry: {
                 type: "Point",
                 coordinates: new ymap.LatLng(item.GeoLong, item.GeoLat),
                balloonContentHeader: item.Model
            }
        });

        myMap.geoObjects.add();

        marker.balloon.open();
    })
});