﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carsharing.Models;

namespace Carsharing.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetData()
        {
             // создадим список данных
            List<Cars> cars = new List<Cars>();
            cars.Add(new Cars() { Id = 1, Model = "Skofa Rapid", GeoLat = 54.30382,  GeoLong = 48.34197, Number = "A103AA"});

            return Json(cars, JsonRequestBehavior.AllowGet);
        }
    }
}